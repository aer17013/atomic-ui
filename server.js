const { countReset } = require("console");
const express = require("express");
const app = express();
const port = 5001;

const fs = require("fs");

let rawdata = fs.readFileSync("tickets.json");

app.get("/api", (req, res) => {
  function parseAndReturn() {
    let tickets = JSON.parse(rawdata);

    try {
      let page = req.query.page || "1";
      let perPage = req.query.per_page || "50";
      let FilterBy = req.query.filter_by || "all";
      let search = req.query.search || "";

      if (isNaN(page) || isNaN(perPage)) {
        return res.sendStatus(400);
      }

      let data = [];
      page = parseInt(page);
      perPage = parseInt(perPage);

      // Search, count, then filterBy: should stay on the same order to keep counts correct
      if (search !== "") {
        tickets = tickets.filter((ticket) =>
          ticket.title.toLowerCase().includes(search.toLowerCase())
        );
      }

      const counts = {
        all: tickets.length,
        open: tickets.slice().filter((ticket) => ticket.status === "open")
          .length,
        feedback: tickets
          .slice()
          .filter((ticket) => ticket.status === "feedback").length,
        resolved: tickets
          .slice()
          .filter((ticket) => ticket.status === "resolved").length,
      };

      if (FilterBy !== "all") {
        tickets = tickets.filter((ticket) => ticket.status === FilterBy);
      }

      data = tickets.slice(
        (page - 1) * perPage,
        (page - 1) * perPage + perPage
      );

      return res.json({
        data,
        page,
        total_pages: Math.ceil(tickets.length / perPage),
        total_items: tickets.length,
        counts,
      });
    } catch (error) {
      console.error(error);
      res.json({ data: "something went wrong" });
    }
  }

  const delay = (0.5 + Math.random() * 2) * 500;

  setTimeout(parseAndReturn, delay);
});

app.listen(port, () => {
  console.log(`App Listening at http://localhost:${port}`);
});
