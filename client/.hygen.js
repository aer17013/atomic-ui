const types = {
  atom: { name: "atoms", path: "components/atoms" },
  molecule: { name: "molecules", path: "components/molecules" },
  pattern: { name: "patterns", path: "components/patterns" },
  screen: { name: "screens", path: "components/screens" },
  template: { name: "templates", path: "components/templates" },
};

module.exports = {
  helpers: {
    getTypePath: (type = "atom") => {
      return types[type].path;
    },

    getType: (type = "atom") => {
      return types[type].name;
    },
  },
};
