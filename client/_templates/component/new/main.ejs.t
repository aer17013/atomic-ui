---
to: src/<%=h.getTypePath(locals.type)%>/<%=name%>/<%=name%>.js
---
import PropTypes from 'prop-types';
import './<%=name%>.scss';
import classnames from "classnames";

export const <%=name%> = ({className}) => {
    const classes = classnames(
        '<%=name%>',
        className
    );
    return <div className={classes}> Hello form <%=name%> </div>;
}

<%=name%>.propTypes = {
    className: PropTypes.string
};

