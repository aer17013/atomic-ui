import { QueryClient, QueryClientProvider } from 'react-query';
import { SupportTickets } from "./components/screens";
import { PageHeader } from "./components/patterns";

const queryClient = new QueryClient()

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <PageHeader />
    <SupportTickets />
    </QueryClientProvider>
  );
}

export default App;
