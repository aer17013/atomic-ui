import PropTypes from "prop-types";
import "./FilterBy.scss";
import classnames from "classnames";
import { ArrowDown } from "../../../assets/icons";
import { MenuItem } from "../../atoms";
import { Menu } from "../Menu/Menu";
import { useState } from "react";
import { useOutsideClick } from "../../../hooks";

// options data soptions:
const defaultOptions = [
  {
    label: "Option 2",
    value: "option2",
    count: 10,
    countVariant: "blue",
  },
  {
    label: "Option 3",
    value: "option3",
    count: 1,
    countVariant: "gray",
  },
  {
    label: "Option 4",
    value: "option4",
    count: 10,
    countVariant: "purple",
  },
];

// countVariant can be one of ["gray", "blue", "green", "purple"]

export const FilterBy = ({
  onChange = () => null,
  value = "option4",
  options = defaultOptions,
  className,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const classes = classnames("filter-by", className);

  const menuRef = useOutsideClick(() => setIsOpen(false));

  const selectedObject = options?.filter(
    (option) => option?.value === value
  )[0];

  return (
    <div className={classes}>
      {!isOpen ? (
        <div
          onClick={(e) => {
            e.stopPropagation();
            setIsOpen(true);
          }}
          className="filter-by__button"
        >
          <MenuItem
            count={selectedObject?.count}
            countVariant={selectedObject?.countVariant}
          >
            {selectedObject?.label}
          </MenuItem>
          <ArrowDown />
        </div>
      ) : (
        <div className="filter-by__menu" ref={menuRef}>
          <Menu>
            {options?.map((option) => (
              <MenuItem
                key={option?.value}
                onClick={() => {
                  onChange(option?.value);
                  setIsOpen(false);
                }}
                count={option?.count}
                countVariant={option?.countVariant}
              >
                {option?.label}
              </MenuItem>
            ))}
          </Menu>
        </div>
      )}
    </div>
  );
};

FilterBy.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.object),
};
