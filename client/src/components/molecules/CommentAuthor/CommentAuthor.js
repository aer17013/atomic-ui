import PropTypes from "prop-types";
import "./CommentAuthor.scss";
import classnames from "classnames";

export const CommentAuthor = ({
  picture,
  text = "Last by {Member Name}",
  className,
}) => {
  const classes = classnames("comment-author", className);
  return (
    <div className={classes}>
      <img
        className="comment-author__avatar"
        src={picture || "https://i.pravatar.cc/"}
        alt="avatar"
      />
      <p className="comment-author__text">{text}</p>
    </div>
  );
};

CommentAuthor.propTypes = {
  className: PropTypes.string,
  picture: PropTypes.string,
  text: PropTypes.string,
};
