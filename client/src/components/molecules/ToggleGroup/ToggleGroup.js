import PropTypes from "prop-types";
import "./ToggleGroup.scss";
import { Label, Toggle } from "../../atoms";
import React from "react";

export const ToggleGroupElement = ({
  optionOne = {},
  optionTwo = {},
  value = "",
  onChange,
  label,
  required,
}) => {
  return (
    <div className="toggle-group">
      <Label required={required}>{label}</Label>
      <Toggle
        optionOne={optionOne}
        optionTwo={optionTwo}
        value={value || optionOne.value}
        onChange={onChange}
      />
    </div>
  );
};

ToggleGroupElement.propTypes = {
  optionOne: PropTypes.object,
  optionTwo: PropTypes.object,
  value: PropTypes.string,
  onChange: PropTypes.func,
  label: PropTypes.string,
  required: PropTypes.bool,
};

export const ToggleGroup = React.memo(ToggleGroupElement);

ToggleGroup.displayName = "ToggleGroup";
