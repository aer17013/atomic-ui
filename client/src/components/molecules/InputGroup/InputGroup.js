import PropTypes from "prop-types";
import "./InputGroup.scss";
import { Label, Text, Input } from "../../atoms";
import React from "react";

export const InputGroupElement = ({
  type = "text",
  label = "",
  placeholder = "",
  helpText = "",
  value = "",
  onChange,
  required,
}) => {
  const name = label.toLowerCase().replace(/\s/g, "-");

  return (
    <div className="input-group">
      <Label required={required} inputName={name}>
        {label}
      </Label>
      <Input
        required={required}
        name={name}
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
      />
      {helpText && <Text fontSize="13px">{helpText}</Text>}
    </div>
  );
};

InputGroupElement.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  helpText: PropTypes.string,
  value: PropTypes.string,
  type: PropTypes.oneOf(["text", "number", "price"]),
  onChange: PropTypes.func,
  required: PropTypes.bool,
};

export const InputGroup = React.memo(InputGroupElement);

InputGroup.displayName = "InputGroup";
