import PropTypes from "prop-types";
import "./Menu.scss";
import classnames from "classnames";
import React from "react";

export const MenuElement = ({ children, className }) => {
  return <div className={classnames("menu", className)}>{children}</div>;
};

MenuElement.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]),
  className: PropTypes.string,
};

export const Menu = React.memo(MenuElement);

Menu.displayName = "Menu";
