import PropTypes from "prop-types";
import "./Pagination.scss";
import classnames from "classnames";
import { Button } from "../../atoms";
import { ArrowRight, ArrowLeft } from "../../../assets/icons";
import { range } from "../../../utils";

export const Pagination = ({
  className,
  perPage,
  setPage = () => null,
  currentPage,
  totalPages,
  totalItems,
}) => {
  const classes = classnames("pagination", className);
  return (
    <div className={classes}>
      <div className="pagination__pages-list">
        {totalPages &&
          range(1, totalPages).map((pageNumber) => (
            <span
              key={pageNumber}
              className={classnames(
                "pagination__page",
                pageNumber === currentPage && "pagination__page--active"
              )}
              onClick={() => setPage(pageNumber)}
            >
              {pageNumber}
            </span>
          ))}
      </div>
      <div className="pagination__actions">
        <span className="pagination__all-pages">
          {perPage * (currentPage - 1) + 1}-
          {currentPage === totalPages ? totalItems : perPage * currentPage} of{" "}
          {totalItems}
        </span>
        <div className="pagination__buttons-wrapper">
          <Button
            className="pagination__button"
            variant="transparent"
            icon={<ArrowLeft />}
            disabled={1 === currentPage}
            onClick={() => setPage(currentPage - 1)}
          ></Button>
          <Button
            className="pagination__button"
            icon={<ArrowRight />}
            disabled={totalPages === currentPage}
            onClick={() => setPage(currentPage + 1)}
          ></Button>
        </div>
      </div>
    </div>
  );
};

Pagination.propTypes = {
  className: PropTypes.string,
  perPage: PropTypes.number,
  setPage: PropTypes.func,
  currentPage: PropTypes.number,
  totalPages: PropTypes.number,
  totalItems: PropTypes.number,
};
