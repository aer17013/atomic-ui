import PropTypes from "prop-types";
import "./ImageInputGroup.scss";
import { Label, Text, ImageInput } from "../../atoms";
import React from "react";

export const ImageInputGroupElement = ({
  label = "",
  helpText = "",
  file = "",
  onChange,
  required,
}) => {
  const name = label.toLowerCase().replace(/\s/g, "-");

  return (
    <div className="image-input-group">
      <Label required={required} inputName={name}>
        {label}
      </Label>
      <ImageInput
        required={required}
        name={name}
        file={file}
        onChange={onChange}
      />
      {helpText && <Text fontSize="13px">{helpText}</Text>}
    </div>
  );
};

ImageInputGroupElement.propTypes = {
  label: PropTypes.string,
  helpText: PropTypes.string,
  file: PropTypes.string,
  onChange: PropTypes.func,
  required: PropTypes.bool,
};

export const ImageInputGroup = React.memo(ImageInputGroupElement);

ImageInputGroup.displayName = "ImageInputGroup";
