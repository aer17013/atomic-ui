import PropTypes from "prop-types";
import "./Link.scss";
import classnames from "classnames";

export const Link = ({
  className,
  href = "#",
  target = "_self",
  color,
  fontSize,
  fontWeight,
  children,
}) => {
  const classes = classnames("link", className);
  const styles = {
    color,
    fontSize,
    fontWeight,
  };
  return (
    <a style={styles} href={href} target={target} className={classes}>
      {children}
    </a>
  );
};

Link.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string,
  target: PropTypes.oneOf(["_self", "_blank", "_parent", "_top"]),
  children: PropTypes.string,
  fontSize: PropTypes.string,
  fontWeight: PropTypes.string,
  color: PropTypes.string,
};
