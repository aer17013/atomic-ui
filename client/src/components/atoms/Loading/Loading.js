import PropTypes from "prop-types";
import "./Loading.scss";

export const Loading = ({ size }) => {
  const style = {
    width: `${size}px`,
    height: `${size}px`,
  };
  return <div style={style} className="loading" />;
};

Loading.propTypes = {
  size: PropTypes.number,
};
