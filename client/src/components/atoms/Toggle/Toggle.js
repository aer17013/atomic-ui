import PropTypes from "prop-types";
import "./Toggle.scss";
import classnames from "classnames";
import React from "react";

const ToggleElement = ({
  optionOne = {},
  optionTwo = {},
  value = "",
  onChange,
}) => {
  const handleClick = (e) => {
    onChange(e.target.getAttribute("data-value"));
  };

  let btnOneClassName = "toggle__button";
  if (value === optionOne.value)
    btnOneClassName = classnames("toggle__button", "toggle__button--active");

  let btnTwoClassName = "toggle__button";
  if (value === optionTwo.value)
    btnTwoClassName = classnames("toggle__button", "toggle__button--active");

  return (
    <div className="toggle">
      <button
        type="button"
        className={btnOneClassName}
        data-value={optionOne?.value}
        onClick={handleClick}
      >
        {optionOne?.label}
      </button>
      <button
        type="button"
        className={btnTwoClassName}
        data-value={optionTwo?.value}
        onClick={handleClick}
      >
        {optionTwo?.label}
      </button>
    </div>
  );
};

ToggleElement.propTypes = {
  optionOne: PropTypes.object,
  optionTwo: PropTypes.object,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

export const Toggle = React.memo(ToggleElement);

Toggle.displayName = "Toggle";
