import PropTypes from "prop-types";
import "./Button.scss";
import classnames from "classnames";
import React from "react";

const ButtonElement = ({
  children,
  onClick,
  variant = "default",
  icon,
  type = "button",
  className,
  iconPosition = "left",
  disabled = false,
}) => {
  const ButtonClassByVariant = {
    gray: "button--gray",
    transparent: "button--transparent",
    blue: "button--blue",
  };

  const ButtonClasses = classnames(
    "button",
    ButtonClassByVariant[variant],
    className
  );

  const iconClasses = classnames(
    "button__icon",
    iconPosition == "right" && "button__icon--right",
    !children && "button__icon--alone"
  );

  return (
    <button
      disabled={disabled}
      type={type}
      className={ButtonClasses}
      onClick={onClick}
    >
      {icon && <span className={iconClasses}>{icon}</span>} {children}
    </button>
  );
};

ButtonElement.propTypes = {
  children: PropTypes.string,
  onClick: PropTypes.func,
  variant: PropTypes.oneOf(["default", "gray", "transparent", "blue"]),
  icon: PropTypes.element,
  type: PropTypes.oneOf(["button", "submit", "reset"]),
  className: PropTypes.string,
  iconPosition: PropTypes.oneOf(["left", "right"]),
  disabled: PropTypes.bool,
};

// Exporting a memoized version of the component
export const Button = React.memo(ButtonElement);

Button.displayName = "Button";
