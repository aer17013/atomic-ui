import PropTypes from "prop-types";
import "./MenuItem.scss";
import classnames from "classnames";

export const MenuItem = ({
  children,
  onClick,
  color,
  count,
  countVariant = "blue",
  className,
}) => {
  const style = {
    color,
  };

  const MenuItemDefaultClassName = "menu-item";
  const MenuItemClasses = classnames(MenuItemDefaultClassName, className);

  const countClassNameByVariant = {
    gray: "menu-item__count--gray",
    blue: "menu-item__count--blue",
    green: "menu-item__count--green",
    purple: "menu-item__count--purple",
  };

  const countClasses = classnames(
    "menu-item__count",
    countClassNameByVariant[countVariant]
  );

  return (
    <div className={MenuItemClasses} style={style} onClick={onClick}>
      <div className="menu-item__text">{children}</div>

      {count && <span className={countClasses}>{count}</span>}
    </div>
  );
};

MenuItem.propTypes = {
  color: PropTypes.string,
  children: PropTypes.string,
  onClick: PropTypes.func,
  count: PropTypes.number,
  countVariant: PropTypes.oneOf(["gray", "blue", "green", "purple"]),
  className: PropTypes.string,
};
