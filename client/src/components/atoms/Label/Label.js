import PropTypes from "prop-types";
import "./Label.scss";
import React from "react";

const LabelElement = ({ inputName = "", children, required }) => {
  return (
    <label className="label" htmlFor={inputName}>
      {children}
      {!required && <span className="label__optional"> (Optional)</span>}
    </label>
  );
};

LabelElement.propTypes = {
  children: PropTypes.string,
  required: PropTypes.bool,
  inputName: PropTypes.string,
};

export const Label = React.memo(LabelElement);

Label.displayName = "Label";
