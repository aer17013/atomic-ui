import PropTypes from "prop-types";
import "./Text.scss";
import classnames from "classnames";
import React from "react";

const TextElement = ({ children, fontSize, color, fontWeight, className }) => {
  const style = {
    fontSize,
    color,
    fontWeight,
  };
  return (
    <p className={classnames("text", className)} style={style}>
      {children}
    </p>
  );
};

TextElement.propTypes = {
  children: PropTypes.string,
  fontSize: PropTypes.string,
  fontWeight: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
};

export const Text = React.memo(TextElement);

Text.displayName = "Text";
