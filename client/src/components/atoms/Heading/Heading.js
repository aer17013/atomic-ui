import PropTypes from "prop-types";
import classnames from "classnames";
import "./Heading.scss";
import React from "react";

const HeadingElement = ({ level = "1", children, className }) => {
  const Tag = `h${level}`;
  return <Tag className={classnames("heading", className)}>{children}</Tag>;
};

HeadingElement.propTypes = {
  level: PropTypes.oneOf(["1", "2", "3", "4", "5", "6"]),
  children: PropTypes.string,
  className: PropTypes.string,
};

export const Heading = React.memo(HeadingElement);

Heading.displayName = "Heading";
