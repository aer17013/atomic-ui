import PropTypes from "prop-types";
import "./IconBox.scss";
import classNames from "classnames";

export const IconBox = ({
  icon,
  backgroundColor,
  size = "medium",
  className,
}) => {
  const classNameBySize = {
    small: "icon-box--small",
    medium: "icon-box--medium",
    large: "icon-box--large",
  };

  const styles = {
    backgroundColor,
  };

  const classes = classNames("icon-box", classNameBySize[size], className);

  return (
    <div style={styles} className={classes}>
      {icon}
    </div>
  );
};

IconBox.propTypes = {
  icon: PropTypes.element.isRequired,
  backgroundColor: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.oneOf(["small", "medium", "large"]),
};
