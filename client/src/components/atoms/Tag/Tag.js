import PropTypes from "prop-types";
import "./Tag.scss";
import classnames from "classnames";

export const Tag = ({ className, icon, variant = "blue", children }) => {
  const classNameByVariant = {
    blue: "tag--blue",
    gray: "tag--gray",
    green: "tag--green",
    purple: "tag--purple",
  };

  const classes = classnames("tag", classNameByVariant[variant], className);

  return (
    <div className={classes}>
      {icon && <span className="tag__icon">{icon}</span>}
      {children}
    </div>
  );
};

Tag.propTypes = {
  children: PropTypes.string,
  icon: PropTypes.element,
  className: PropTypes.string,
  variant: PropTypes.oneOf(["blue", "green", "purple", "gray"]),
};
