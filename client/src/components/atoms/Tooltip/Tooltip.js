import PropTypes from "prop-types";
import "./Tooltip.scss";
import classnames from "classnames";

export const Tooltip = ({ className, children }) => {
  const classes = classnames("tooltip", className);
  return <div className={classes}> {children} </div>;
};

Tooltip.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};
