import classNames from "classnames";
import PropTypes from "prop-types";
import "./Input.scss";
import classnames from "classnames";
import React from "react";
import { Search, Close } from "../../../assets/icons";

const InputElement = ({
  type = "text",
  width = "353px",
  placeholder = "",
  value = "",
  onChange = () => null,
  name = "",
  className,
  required,
}) => {
  const style = {
    width,
  };

  const defaultWrapperClassname = "input-wrapper";

  const wrapperClassByType = {
    price: "input-wrapper--price",
    search: "input-wrapper--search",
  };

  const wrapperClasses = classnames(
    defaultWrapperClassname,
    wrapperClassByType[type],
    className
  );

  const inputTypes = {
    text: "text",
    number: "number",
    price: "number",
    search: "text",
  };

  return (
    <div className={wrapperClasses} style={style}>
      {type === "search" && (
        <>
          <span className="input-wrapper__search-icon">
            <Search />
          </span>

          {value && (
            <span
              onClick={() => onChange("")}
              className="input-wrapper__search-clear"
            >
              <Close />
            </span>
          )}
        </>
      )}
      <input
        required={required}
        className="input-wrapper__input"
        type={inputTypes[type]}
        id={name}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={(event) => onChange(event.target.value)}
      />
    </div>
  );
};

const possibleTypes = ["text", "number", "price", "search"];

InputElement.propTypes = {
  name: PropTypes.string,
  width: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  type: PropTypes.oneOf(possibleTypes),
  onChange: PropTypes.func,
  className: PropTypes.string,
  required: PropTypes.bool,
};

export const Input = React.memo(InputElement);

Input.displayName = "Input";
