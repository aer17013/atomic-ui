import PropTypes from "prop-types";
import { useState, useRef } from "react";
import { Loading } from "../Loading";
import classnames from "classnames";
import { Plus } from "../../../assets/icons";
import "./ImageInput.scss";

export const ImageInput = ({ file, onChange, name = "", required = false }) => {
  const [isLoading, setLoading] = useState(false);

  const inputEl = useRef(null);

  const handleChange = (e) => {
    if (e.target.files[0]) {
      setLoading(true);
      // #TO_REMOVE_IN_PRODUCTION: setTimeout is added just to to preview the upload state of the component
      setTimeout(() => {
        onChange(URL.createObjectURL(e.target.files[0]));
        setLoading(false);
        e.target.value = null;
      }, 1000);
    }
  };

  let buttonClassName = "image-input__button";
  if (isLoading)
    buttonClassName = classnames(
      "image-input__button",
      "image-input__button--loading"
    );

  return (
    <div className="image-input">
      <div className="image-input__button-group">
        {/** Main Button */}
        <button
          type="button"
          className={buttonClassName}
          disabled={isLoading || file}
          onClick={() => inputEl.current.click()}
        >
          {isLoading ? (
            <Loading />
          ) : file ? (
            <img className="image-input__image" alt="Preview" src={file} />
          ) : (
            <Plus />
          )}
        </button>

        {/** Edit & Remove Image Buttons */}
        {file && !isLoading && (
          <div className="image-input__actions">
            <button
              type="button"
              className="image-input__edit"
              onClick={() => inputEl.current.click()}
            >
              Edit
            </button>
            <button
              type="button"
              className="image-input__delete"
              onClick={() => {
                URL.revokeObjectURL(file);
                onChange();
              }}
            >
              Remove
            </button>
          </div>
        )}
      </div>
      <input
        required={required}
        name={name}
        ref={inputEl}
        className="image-input__input"
        type="file"
        onChange={handleChange}
        accept="image/*"
      />
    </div>
  );
};

ImageInput.propTypes = {
  file: PropTypes.string,
  onChange: PropTypes.func,
  name: PropTypes.string,
  required: PropTypes.bool,
};
