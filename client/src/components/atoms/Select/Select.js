import PropTypes from "prop-types";
import "./Select.scss";
import React from "react";

const defaultOptions = [
  {
    value: "weeks",
    label: "Week(s)",
  },
  {
    value: "months",
    label: "Month(s)",
  },
  {
    value: "years",
    label: "Year(s)",
  },
];

const SelectElement = ({
  options = defaultOptions,
  width = "168px",
  value = "months",
  onChange,
  required,
}) => {
  const style = {
    width,
  };

  return (
    <select
      required={required}
      value={value}
      onChange={(e) => onChange(e.target.value)}
      className="select"
      style={style}
    >
      {options.map((option) => (
        <option key={option?.value} value={option?.value}>
          {option?.label}
        </option>
      ))}
    </select>
  );
};

SelectElement.propTypes = {
  options: PropTypes.array,
  value: PropTypes.string,
  onChange: PropTypes.func,
  width: PropTypes.string,
  required: PropTypes.bool,
};

export const Select = React.memo(SelectElement);

Select.displayName = "Select";
