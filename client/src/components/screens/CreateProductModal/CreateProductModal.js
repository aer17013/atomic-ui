import "./CreateProductModal.scss";
import { GeneralInfo, PricingPlans, Accordion } from "../../patterns";
import { Heading, Button, Portal } from "../../atoms";
import { useProduct } from "../../../hooks";
import { useEffect, useRef, useState } from "react";
import React from "react";

export const CreateProductModalElement = () => {
  const {
    name,
    file,
    plans,
    setName,
    setFile,
    addPlan,
    updatePlan,
    duplicatePlan,
    archivePlan,
    createProduct,
  } = useProduct();

  // React Portal is used to fix the issue of having nested forms
  const [pricingPlansPortal, setPricingPlansPortal] = useState();

  const portalRef = useRef(null);

  useEffect(() => {
    setPricingPlansPortal(portalRef.current);
  }, [portalRef]);

  return (
    <>
      <form
        id="create-product-modal"
        onSubmit={(e) => {
          e.preventDefault();
          createProduct();
        }}
        className="create-product-modal"
      >
        <Heading level="2" className="create-product-modal__heading">
          Create Product or Service
        </Heading>

        <GeneralInfo
          file={file}
          onChangeFile={setFile}
          name={name}
          onChangeName={setName}
        />

        <div ref={portalRef} />

        <div className="create-product-modal__footer">
          <Button className="create-product-modal__footer-cancel-button">
            Cancel
          </Button>
          <Button type="submit" variant="gray">
            Create
          </Button>
        </div>
      </form>

      <Portal container={pricingPlansPortal}>
        <PricingPlans>
          <Accordion
            addPlan={addPlan}
            duplicatePlan={duplicatePlan}
            archivePlan={archivePlan}
            plans={plans}
            updatePlan={updatePlan}
          />
        </PricingPlans>
      </Portal>
    </>
  );
};

export const CreateProductModal = React.memo(CreateProductModalElement);

CreateProductModal.displayName = "CreateProductModal";
