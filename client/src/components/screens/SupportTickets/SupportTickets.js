import PropTypes from "prop-types";
import "./SupportTickets.scss";
import classnames from "classnames";
import { TicketsSection, SupportResources, TicketsTable } from "../../patterns";

export const SupportTickets = ({ className }) => {
  const classes = classnames("SupportTickets", className);
  return (
    <div className={classes}>
      <SupportResources />
      <TicketsSection />
    </div>
  );
};

SupportTickets.propTypes = {
  className: PropTypes.string,
};
