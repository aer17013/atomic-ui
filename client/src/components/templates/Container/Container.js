import PropTypes, { array } from "prop-types";
import "./Container.scss";
import classnames from "classnames";

export const Container = ({ className, children }) => {
  const classes = classnames("container", className);
  return <div className={classes}> {children}</div>;
};

Container.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]),
};
