import PropTypes from "prop-types";
import "./Table.scss";
import classnames from "classnames";

export const Table = ({ className, children }) => {
  const classes = classnames("table", className);
  return <table className={classes}>{children}</table>;
};

Table.Thead = ({ children, className }) => {
  return (
    <thead className={classnames("table__thead", className)}>{children}</thead>
  );
};

Table.Tbody = ({ children, className }) => {
  return (
    <tbody className={classnames("table__tbody", className)}>{children}</tbody>
  );
};

Table.Tr = ({ children, className }) => {
  return <tr className={classnames("table__tr", className)}>{children}</tr>;
};

Table.Th = ({ children, className }) => {
  return <th className={classnames("table__th", className)}>{children}</th>;
};

Table.Td = ({ children, className, dataTitle }) => {
  return (
    <td data-title={dataTitle} className={classnames("table__td", className)}>
      {children}
    </td>
  );
};

Table.propTypes = {
  className: PropTypes.string,
};
