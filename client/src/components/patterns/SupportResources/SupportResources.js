import PropTypes from "prop-types";
import "./SupportResources.scss";
import classnames from "classnames";
import { Heading, Link, Button, IconBox, Text, Input } from "../../atoms";
import { Container } from "../../templates";
import { Messages, Support, LongArrowRight } from "../../../assets/icons";

export const SupportResources = ({ className }) => {
  const classes = classnames("support-resources", className);
  return (
    <Container className={classes}>
      <div className="support-resources__header">
        <Heading level="5" className="support-resources__section-title">
          Support Resources
        </Heading>
        <div className="support-resources__section-support">
          <Link href="#">Need Help?</Link>
          <Button variant="blue" icon={<Support />}>
            Get Support
          </Button>
        </div>
      </div>

      <div className="support-resources__body">
        <IconBox icon={<Messages />} variant="large" />
        <Heading level="2">Support Forum</Heading>
        <Text>Search the topic you need help with in our support forums.</Text>
        <Button
          variant="transparent"
          iconPosition="right"
          icon={<LongArrowRight />}
          className="support-resources__forums-button"
        >
          Browse Forums
        </Button>
        <Input type="search" placeholder="Search support forum" />
      </div>
    </Container>
  );
};

SupportResources.propTypes = {
  className: PropTypes.string,
};
