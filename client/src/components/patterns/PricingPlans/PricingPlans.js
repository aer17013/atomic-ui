import PropTypes from "prop-types";
import "./PricingPlans.scss";
import { Heading, Text } from "../../atoms";
import React from "react";

export const PricingPlansElement = ({ children }) => {
  return (
    <div className="pricing-plans">
      <Heading level="4" className="pricing-plans__heading">
        Pricing Plans
      </Heading>
      <Text className="pricing-plans__text">
        Create pricing plans for this product/service. Note that every
        product/service can have multiple plans.
      </Text>
      {children}
    </div>
  );
};

PricingPlansElement.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]),
};

export const PricingPlans = React.memo(PricingPlansElement);

PricingPlans.displayName = "PricingPlans";
