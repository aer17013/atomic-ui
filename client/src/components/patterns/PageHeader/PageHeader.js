import PropTypes from "prop-types";
import "./PageHeader.scss";
import classnames from "classnames";

export const PageHeader = ({ className }) => {
  const classes = classnames("page-header", className);
  return <div className={classes}></div>;
};

PageHeader.propTypes = {
  className: PropTypes.string,
};
