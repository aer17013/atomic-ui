import PropTypes from "prop-types";
import "./AccordionItem.scss";
import classnames from "classnames";
import { PlanForm } from "../PlanForm";
import { ArrowDown, ArrowUp, HorizontalDots } from "../../../assets/icons";
import { Menu } from "../../molecules";
import { MenuItem } from "../../atoms";
import { useState } from "react";
import { useOutsideClick } from "../../../hooks";
import React from "react";

const AccordionItemElement = ({
  plan,
  addPlan,
  updatePlan,
  isOpen = false,
  onOpen,
  setOpenID,
  duplicatePlan,
  archivePlan,
}) => {
  const { status, name, price } = plan;

  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const menuRef = useOutsideClick(() => setIsMenuOpen(false));

  const handleItemToggle = () => {
    if (!isOpen) {
      onOpen(plan.id);
    } else {
      onOpen(null);
    }
  };

  return (
    <div className="accordion-item">
      <div className="accordion-item__header">
        {plan.name && (
          <>
            <div
              className={classnames(
                "accordion-item__header-element",
                "accordion-item__header-element--name"
              )}
            >
              {name}
            </div>
            <div
              className={classnames(
                "accordion-item__header-element",
                "accordion-item__header-element--status"
              )}
            >
              {status}
            </div>
            <div
              className={classnames(
                "accordion-item__header-element",
                "accordion-item__header-element--price"
              )}
            >{`$${Number(price).toFixed(2)}`}</div>
            <div
              className={classnames(
                "accordion-item__header-element",
                "accordion-item__header-element--icon"
              )}
            >
              <div ref={menuRef}>
                <div onClick={() => setIsMenuOpen(true)}>
                  <HorizontalDots />
                </div>
                {isMenuOpen && (
                  <Menu className="accordion-item__menu">
                    <MenuItem
                      onClick={() => {
                        duplicatePlan(plan.id);
                        setIsMenuOpen(false);
                        setOpenID(null);
                      }}
                    >
                      Duplicate Plan
                    </MenuItem>
                    <MenuItem
                      color="#F9393B"
                      onClick={() => {
                        archivePlan(plan.id);
                        setIsMenuOpen(false);
                        setOpenID(null);
                      }}
                    >
                      Archive
                    </MenuItem>
                  </Menu>
                )}
              </div>
            </div>
            <div
              className={classnames(
                "accordion-item__header-element",
                "accordion-item__header-element--icon"
              )}
            >
              {isOpen ? (
                <div onClick={handleItemToggle}>
                  <ArrowUp />
                </div>
              ) : (
                <div onClick={handleItemToggle}>
                  <ArrowDown />
                </div>
              )}
            </div>
          </>
        )}
      </div>
      {isOpen && (
        <div className="accordion-item__body">
          <PlanForm
            setOpenID={setOpenID}
            initialPlan={plan}
            action="update"
            updatePlan={updatePlan}
            addPlan={addPlan}
          />
        </div>
      )}
    </div>
  );
};

AccordionItemElement.propTypes = {
  plan: PropTypes.object,
  isOpen: PropTypes.bool,
  addPlan: PropTypes.func,
  updatePlan: PropTypes.func,
  onOpen: PropTypes.func,
  setOpenID: PropTypes.func,
  duplicatePlan: PropTypes.func,
  archivePlan: PropTypes.func,
};

export const AccordionItem = React.memo(AccordionItemElement);

AccordionItem.displayName = "AccordionItem";
