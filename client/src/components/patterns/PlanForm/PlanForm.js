import PropTypes from "prop-types";
import "./PlanForm.scss";
import { InputGroup, ToggleGroup } from "../../molecules";
import { Label, Select, Input, Button } from "../../atoms";
import { usePricingPlan } from "../../../hooks";
import { Plus } from "../../../assets/icons";
import React, { useRef } from "react";

export const PlanFormElement = ({
  addPlan,
  updatePlan,
  action = "create",
  setOpenID,
  initialPlan,
}) => {
  const {
    name,
    price,
    billingType,
    period,
    periodUnit,
    billingCycles,
    setName,
    setPrice,
    updateBillingType,
    setPeriod,
    setPeriodUnit,
    setBillingCycles,
    resetPlan,
  } = usePricingPlan(initialPlan);

  const ButtonsByAction = {
    create: (
      <div className="plan-form__footer">
        <Button type="submit" icon={<Plus />}>
          Add Another Plan
        </Button>
      </div>
    ),

    update: (
      <div className="plan-form__footer">
        <Button type="submit">Save</Button>
        <Button
          variant="gray"
          onClick={() => {
            setOpenID(null);
          }}
        >
          Cancel
        </Button>
      </div>
    ),
  };

  const billingTypeOne = useRef({
    label: "Recurring",
    value: "recurring",
  }).current;
  const billingTypeTwo = useRef({
    label: "One time",
    value: "one-time",
  }).current;

  const handleSubmit = (e) => {
    e.preventDefault();

    const plan = {
      id: initialPlan?.id,
      name,
      price,
      billingType,
      period,
      periodUnit,
      billingCycles,
      status: "active",
    };

    if (action === "create") {
      addPlan(plan);
      resetPlan();
      return;
    }

    if (action === "update") {
      updatePlan(plan.id, plan);
      setOpenID(null);
      return;
    }
    return console.error(
      "PlanForm action type is not valid, it should be either 'create' or 'update"
    );
  };

  return (
    <form id="plan-form" onSubmit={handleSubmit} className="plan-form">
      <div className="plan-form__row">
        <div className="plan-form__column">
          <InputGroup
            required={true}
            value={name}
            onChange={setName}
            label="Plan Name"
            placeholder="E.g. Monthly, Lifetime, etc."
          />
          <InputGroup
            required={true}
            value={price}
            onChange={setPrice}
            label="Plan Price"
            type="price"
            placeholder="0.00"
          />
        </div>

        <div className="plan-form__column">
          <ToggleGroup
            required={true}
            label="Billing Type"
            value={billingType}
            onChange={updateBillingType}
            optionOne={billingTypeOne}
            optionTwo={billingTypeTwo}
          />
          {billingType === "recurring" && (
            <>
              <Label required={true}>Bill Every</Label>
              <div className="plan-form__input-select-group">
                <Input
                  required={true}
                  value={period}
                  onChange={setPeriod}
                  className="plan-form__input"
                  width="168px"
                  type="number"
                  placeholder="1"
                />
                <Select
                  required={true}
                  value={periodUnit}
                  onChange={setPeriodUnit}
                />
              </div>
            </>
          )}
        </div>

        <div className="plan-form__billing-type">
          {billingType === "recurring" && (
            <InputGroup
              required={false}
              value={billingCycles}
              onChange={setBillingCycles}
              helpText="Leave this empty to auto-renew this plan until canceled."
              label="No. of Billing Cycles"
              optional={true}
              type="number"
              placeholder="E.g. 6, 12, etc."
            />
          )}
        </div>
      </div>
      {ButtonsByAction[action]}
    </form>
  );
};

PlanFormElement.propTypes = {
  addPlan: PropTypes.func,
  updatePlan: PropTypes.func,
  action: PropTypes.oneOf(["create", "update"]),
  setOpenID: PropTypes.func,
  initialPlan: PropTypes.object,
};

export const PlanForm = React.memo(PlanFormElement);

PlanForm.displayName = "PlanForm";
