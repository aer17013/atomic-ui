import PropTypes from "prop-types";
import "./GeneralInfo.scss";
import { Heading } from "../../atoms";
import { InputGroup, ImageInputGroup } from "../../molecules";

export const GeneralInfo = ({ name, onChangeName, file, onChangeFile }) => {
  return (
    <div className="general-info">
      <Heading level="4" className="general-info__heading">
        General Info
      </Heading>
      <div className="general-info__row">
        <div className="general-info__column">
          <InputGroup
            required={true}
            value={name}
            onChange={onChangeName}
            label="Product Name"
            placeholder="E.g. Website Maintainance, SEO, etc."
          />
        </div>
        <div className="general-info__column">
          <ImageInputGroup
            file={file}
            onChange={onChangeFile}
            label="Product Image"
            required={false}
            helpText="Upload a sqaure image that doesn’t exceed 2 MB."
          />
        </div>
      </div>
    </div>
  );
};

GeneralInfo.propTypes = {
  name: PropTypes.string,
  onChangeName: PropTypes.func,
  file: PropTypes.string,
  onChangeFile: PropTypes.func,
};
