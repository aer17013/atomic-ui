import PropTypes from "prop-types";
import "./Accordion.scss";
import { PlanForm } from "../PlanForm";
import { AccordionItem } from "../AccordionItem";
import { useState } from "react";
import React from "react";

const AccordionElement = ({
  plans = [],
  addPlan,
  updatePlan,
  archivePlan,
  duplicatePlan,
}) => {
  const [openID, setOpenID] = useState();

  let elements = [];

  if (plans.length > 0) {
    elements = plans.map((plan) => (
      <AccordionItem
        isOpen={plan.id === openID}
        onOpen={(id) => setOpenID(id)}
        key={plan.id}
        setOpenID={setOpenID}
        plan={plan}
        addPlan={addPlan}
        updatePlan={updatePlan}
        archivePlan={archivePlan}
        duplicatePlan={duplicatePlan}
      />
    ));
  }

  return (
    <div className="accordion">
      {elements}
      {!openID && (
        <PlanForm
          setOpenID={setOpenID}
          addPlan={addPlan}
          updatePlan={updatePlan}
          action="create"
        />
      )}
    </div>
  );
};

AccordionElement.propTypes = {
  plans: PropTypes.array,
  addPlan: PropTypes.func,
  updatePlan: PropTypes.func,
  archivePlan: PropTypes.func,
  duplicatePlan: PropTypes.func,
};

export const Accordion = React.memo(AccordionElement);

Accordion.displayName = "Accordion";
