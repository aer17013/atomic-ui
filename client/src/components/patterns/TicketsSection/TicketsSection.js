import PropTypes from "prop-types";
import "./TicketsSection.scss";
import classnames from "classnames";
import { Heading, Input } from "../../atoms";
import { FilterBy, Pagination } from "../../molecules";
import { TicketsTable } from "../TicketsTable/TicketsTable";
import { useFetchedTickets } from "../../../hooks";
import { Container } from "../../templates";

const perPage = 10;

export const TicketsSection = ({ className }) => {
  const {
    isLoading,
    isError,
    error,
    tickets,
    totalPages,
    totalItems,
    setCurrentPage,
    isFetching,
    currentPage,
    search,
    handleSearchChange,
    filter,
    handleFilterChange,
    counts,
  } = useFetchedTickets({ perPage });

  const filterOptions = [
    {
      label: "All",
      value: "all",
      count: counts?.all,
      countVariant: "gray",
    },
    {
      label: "Open",
      value: "open",
      count: counts?.open,
      countVariant: "blue",
    },
    {
      label: "Feedback",
      value: "feedback",
      count: counts?.feedback,
      countVariant: "purple",
    },
    {
      label: "Resolved",
      value: "resolved",
      count: counts?.resolved,
      countVariant: "green",
    },
  ];

  const classes = classnames("tickets-section", className);

  return (
    <Container className={classes}>
      <div className="tickets-section__header">
        <Heading level="5" className="tickets-section__section-title">
          My Tickets
        </Heading>
        <FilterBy
          value={filter || "all"}
          options={filterOptions}
          onChange={handleFilterChange}
        />
        <Input
          width="240px"
          type="search"
          placeholder="Search tickets"
          value={search}
          onChange={handleSearchChange}
        />
      </div>
      <div className="tickets-section__body">
        <TicketsTable
          isLoading={isLoading}
          tickets={tickets}
          totalItems={totalItems}
          totalPages={totalPages}
          isError={isError}
          error={error}
          isFetching={isFetching}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          perPage={perPage}
          search={search}
        />
      </div>
      {!isLoading && !isError && !isFetching && tickets.length > 0 && (
        <Pagination
          className="tickets-section__pagination"
          currentPage={currentPage}
          setPage={setCurrentPage}
          totalPages={totalPages}
          perPage={perPage}
          totalItems={totalItems}
        />
      )}
    </Container>
  );
};

TicketsSection.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]),
};
