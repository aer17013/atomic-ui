import PropTypes from "prop-types";
import "./TicketsTable.scss";
import classnames from "classnames";
import { Table } from "../../templates";
import {
  Link,
  Text,
  Tag,
  Tooltip,
  Loading,
  IconBox,
  Heading,
} from "../../atoms";
import { CommentAuthor } from "../../molecules";
import { Staff, CalenderCheck, Telescope } from "../../../assets/icons";

export const TicketsTable = ({
  className,
  isLoading,
  tickets,

  isError,
  error,
  isFetching,

  search,
}) => {
  const tableIsEmpty =
    isLoading || isFetching || isError || tickets?.length === 0;

  const classes = classnames(
    "tickets-table",
    tableIsEmpty && "tickets-table--empty",
    className
  );

  const tagVariantByStatus = {
    open: "blue",
    feedback: "purple",
    resolved: "green",
  };

  if ("" !== search && tickets?.length === 0) {
    return (
      <div className={classes}>
        <div className="tickets-table__no-tickets">
          <IconBox
            icon={<Telescope />}
            size="large"
            backgroundColor="#EAF1FE"
            className="tickets-table__no-tickets-icon"
          />
          <Heading level="2">{`No tickets found for "${search}"`}</Heading>
          <Text>Please adjust your search term and try again.</Text>
        </div>
      </div>
    );
  }

  if (tickets?.length === 0) {
    return (
      <div className={classes}>
        <div className="tickets-table__no-tickets">
          <IconBox
            icon={<CalenderCheck />}
            size="large"
            backgroundColor="#EAF1FE"
            className="tickets-table__no-tickets-icon"
          />
          <Heading level="2">No Tickets Found!</Heading>
          <Text>
            Your support tickets or feature requests will appear here.
          </Text>
        </div>
      </div>
    );
  }

  if (isLoading || isFetching) {
    return (
      <div className={classes}>
        <Loading size={50} />
      </div>
    );
  }

  if (isError) {
    return (
      <div className={classes}>
        <Text color="red">{error.message}</Text>
      </div>
    );
  }

  return (
    <div className={classes}>
      <Table>
        <Table.Thead>
          <Table.Tr>
            <Table.Th>tickets</Table.Th>
            <Table.Th>status</Table.Th>
            <Table.Th>created on</Table.Th>
            <Table.Th>replies</Table.Th>
          </Table.Tr>
        </Table.Thead>
        <Table.Tbody>
          {tickets?.map((ticket) => (
            <Table.Tr key={ticket.id}>
              <Table.Td dataTitle="ticket">
                <Link color="#286EF1" fontSize="15px" fontWeight="700">
                  {ticket?.title}
                </Link>
                <Text color="#555555" fontWeight="500">
                  {ticket?.forum?.title}
                </Text>
              </Table.Td>
              <Table.Td dataTitle="status" className="tickets-table__status">
                <Tag variant={tagVariantByStatus[ticket?.status]}>
                  {ticket?.status}
                </Tag>
              </Table.Td>
              <Table.Td dataTitle="created on">
                <Text color="#555555" fontSize="13px" fontWeight="400">
                  Today @ 12:04 AM
                </Text>
              </Table.Td>
              <Table.Td
                dataTitle="reply"
                className="tickets-table__comment-author-td"
              >
                <CommentAuthor
                  text={`Last By ${ticket?.latest_post_author?.display_name}`}
                />
                <div className="tickets-table__comment-author-tags">
                  {ticket?.num_replies >= 1 && (
                    <Tooltip>{ticket?.num_replies}</Tooltip>
                  )}
                  {ticket?.latest_post_author?.is_staff && (
                    <Tag variant="gray" icon={<Staff />}>
                      Staff
                    </Tag>
                  )}
                </div>
              </Table.Td>
            </Table.Tr>
          ))}
        </Table.Tbody>
      </Table>
    </div>
  );
};

TicketsTable.propTypes = {
  className: PropTypes.string,
};
