import { useState, useCallback } from "react";
import { generateID } from "../../utils";

export const useProduct = () => {
  const [name, setName] = useState("");
  const [file, setFile] = useState("");
  const [plans, setPlans] = useState([]);

  const createProduct = async () => {
    // Makes post request & send the new product JSON object
    console.log({ name, file, plans });
    // To upload the file
    //const blob = await fetch(blobUrl).then(res => res.blob())
    alert("A new product has been added to the database");
  };

  const addPlan = useCallback(
    (plan) => {
      const pricingPlans = plans.slice();

      pricingPlans.push({ ...plan, id: generateID() });

      setPlans(pricingPlans);
    },
    [plans]
  );

  const getPlanById = useCallback(
    (id) => {
      return plans.filter((plan) => plan.id === id)[0];
    },
    [plans]
  );

  const updatePlan = useCallback(
    (id, plan) => {
      let pricingPlans = plans.slice();

      pricingPlans = pricingPlans.map((obj) => (obj.id === id ? plan : obj));

      setPlans(pricingPlans);
    },
    [plans]
  );

  const archivePlan = useCallback(
    (id) => {
      let pricingPlans = plans.slice();

      pricingPlans = pricingPlans.filter((obj) => obj.id !== id);

      setPlans(pricingPlans);
    },
    [plans]
  );

  const duplicatePlan = useCallback(
    (id) => {
      let plan = getPlanById(id);

      plan = {
        ...plan,
        id: generateID(),
      };

      let pricingPlans = plans.slice();

      pricingPlans.push(plan);

      setPlans(pricingPlans);
    },
    [plans, getPlanById]
  );

  return {
    name,
    file,
    plans,
    setName,
    setFile,
    createProduct,
    addPlan,
    archivePlan,
    duplicatePlan,
    updatePlan,
  };
};
