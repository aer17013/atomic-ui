import { useState, useCallback } from "react";

const defaultPlan = {
  name: "",
  price: "",
  billingType: "recurring",
  period: "",
  periodUnit: "",
  billingCycles: "",
};

export const usePricingPlan = (initialPlan = defaultPlan) => {
  const [name, setName] = useState(initialPlan.name);
  const [price, setPrice] = useState(initialPlan.price);
  const [billingType, setBillingType] = useState(initialPlan.billingType);
  const [period, setPeriod] = useState(initialPlan.period);
  const [periodUnit, setPeriodUnit] = useState(initialPlan.periodUnit);
  const [billingCycles, setBillingCycles] = useState(initialPlan.billingCycles);

  const resetPlan = useCallback(() => {
    setName("");
    setPrice("");
    setBillingCycles("");
    setPeriod("");
    setPeriod("");
    setPeriodUnit("");
    setBillingType("recurring");
  }, []);

  const updateBillingType = useCallback((value) => {
    setPeriod("");
    setPeriodUnit("");
    setBillingCycles("");

    setBillingType(value);
  }, []);

  return {
    name,
    price,
    billingType,
    period,
    periodUnit,
    billingCycles,
    setName,
    setPrice,
    updateBillingType,
    setBillingCycles,
    setPeriod,
    setPeriodUnit,
    resetPlan,
  };
};
