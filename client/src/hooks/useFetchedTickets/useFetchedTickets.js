import { useState, useCallback } from "react";
import { useQuery } from "react-query";
import { useDebouncedValue } from "../../hooks";

export const useFetchedTickets = ({ perPage }) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState("");
  const [filter, setFilter] = useState("");
  const [debouncedSearch, setDebouncedSearch] = useDebouncedValue(search, 500);

  const fetchTickets = useCallback(
    async ({ page, filterBy, search }) => {
      let url = "/api";
      url += `?page=${page}`;
      url += `&per_page=${perPage || 1}`;
      url += `&filter_by=${filterBy || "all"}`;
      if (search) {
        url += `&search=${search}`;
      }
      const res = await fetch(url);
      return res.json();
    },
    [perPage]
  );

  const { isLoading, isError, error, data, isFetching } = useQuery(
    ["tickets", { currentPage, debouncedSearch, filter }],
    () =>
      fetchTickets({
        page: currentPage,
        search: debouncedSearch,
        filterBy: filter,
      })
  );

  const handleSearchChange = useCallback(
    (value) => {
      setSearch(value);
      setCurrentPage(1);
      setDebouncedSearch(value);
    },
    [setDebouncedSearch]
  );

  const handleFilterChange = useCallback((value) => {
    setFilter(value);
    setCurrentPage(1);
  }, []);

  const tickets = data?.data;
  const totalPages = data?.total_pages;
  const totalItems = data?.total_items;
  const counts = data?.counts;

  return {
    isLoading,
    isError,
    error,
    tickets,
    totalPages,
    totalItems,
    setCurrentPage,
    isFetching,
    currentPage,
    search,
    handleSearchChange,
    filter,
    handleFilterChange,
    counts,
  };
};
