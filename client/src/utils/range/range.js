/**
 * Returns an array of numbers from start to end
 * @param  {[Number]} start
 * @param  {[Number]} end
 * @return {[Array]}  [Array of numbers]
 */
export const range = (start, end) =>
  [...Array(end - start + 1)].map((_, i) => start + i);
