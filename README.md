# Atomic UI

Atomic UI is a React component library that is built using the Atomic Design Methodology, With Atomic UI, developers can easily create complex and scalable user interfaces using a collection of simple building blocks. This project showcases my strategy for building UI libraries, which focuses on creating modular, flexible, and easy-to-use components that can be adapted to different projects and design systems.

I have used the library to build two pages to showcase how the components are combined together to build complex UIs, you can find them on the `client/components/screens` folder

## Installtion

The project is composed of two services,

- A client which has the React application and the components library.
- A server with a single API route built with NodeJS & Express, and the data is served from the JSON file `tickets.json`

You will need to install dependencies for both services.<br/>
**Client:** from within the client folder run the command:

```
npm install
```

**Server**
from within the root directory, run

```
npm install
```

After installation you can run the application using the following command from within the root folder:

```
npm run dev
```

This will run both the client & server at the same time.

![Untitled](https://user-images.githubusercontent.com/39377174/191608152-4c62ccbd-3ef5-4ea3-86ac-36ddb761df7c.gif)

# The components library

The components library now has 35 components and they are documented using storybook, the system follows [the atomic design methodology](https://bradfrost.com/blog/post/atomic-web-design/)
to preview the library, run the following command from within `client` folder:

```
npm run storybook
```

![Untitled](https://user-images.githubusercontent.com/39377174/191610925-08d93c38-d7f4-4c66-b368-5bdcd9e83d0d.gif)

## The CLI tool

The project uses [hygen](https://www.hygen.io/) to generate components files and folders with pre-filled templates. to use the cli tool you will need first to install `hygen` gloabally or use `npx`, follow the instructions [here](https://www.hygen.io/docs/quick-start).<br/>
To create a new component run the following command from within the `client` folder:

```
hygen component new {ComponentName} --type {category}
```

The category could be one of the followings: `atom` `molecule` `pattern` `template` or `screen`

The tool helps avoiding redundant tasks when creating new components.
